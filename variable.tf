
variable "instance_type" {
  type = map(any)
  default = {
    "default" = "t2.micro"
    "dev"     = "t2.nano"
    "prod"    = "t2.small"
  }
}
