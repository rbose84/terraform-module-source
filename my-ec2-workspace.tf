resource "aws_instance" "web" {
  ami           = "ami-02f26adf094f51167"
  instance_type = lookup(var.instance_type, terraform.workspace)
  tags = {
    Name = "web"
  }
}