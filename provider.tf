terraform {
  required_version = "< 14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
  #region = var.region-test

}
